import React from 'react';
import axios from 'axios';
import ListItem from './ListItem';
import Modal from './Modal';
import FillItem from './FillItem';

const baseURL = 'http://192.168.1.196:5000';

class Main extends React.Component {
  state = {
    chores: null,
    isEditModalOpen: false,
    isCreateModalOpen: false,
    editChoreId: 0,
    prevPage: false,
    nextPage: false,
    page: 1,
    filter: null,
  };

  handleCompleteButton = async (id, newCompletedValue) => {
    await axios.put(`${baseURL}/api/chores/${id}`, {
      completed: newCompletedValue,
    });
    this.componentDidMount();
  };

  handleDeleteButton = async (id) => {
    await axios.delete(`${baseURL}/api/chores/${id}`);
    this.componentDidMount();
  };

  handleCreate = async (values) => {
    const body = {
      title: values.title,
      description: values.description,
      date: values.date._d.toLocaleString('lt-LT').replace(' ', 'T'),
      completed: false,
    };
    await axios.post(`${baseURL}/api/chores`, body);
    this.setState({
      isCreateModalOpen: false,
    });
    this.componentDidMount();
  };

  editItemButtonClick = (id) => {
    this.setState({
      editChoreId: id,
      isEditModalOpen: true,
    });
  };

  handleEdit = async (values) => {
    const id = this.state.editChoreId;
    if (!id) return;
    else {
      const body = {
        title: values.title,
        description: values.description,
        date: values.date._d.toLocaleString('lt-LT').replace(' ', 'T'),
      };
      await axios.put(`${baseURL}/api/chores/${id}`, body);
      this.setState({
        isEditModalOpen: false,
        editChoreId: 0,
      });
    }
    this.componentDidMount();
  };

  componentDidMount = async () => {
    const completedQuery =
      this.state.filter === null ? '' : this.state.filter ? 'true' : 'false';
    const response = await axios.get(
      `${baseURL}/api/chores?page=${this.state.page}&completed=${completedQuery}`,
    );
    if (response.status === 204 && this.state.page > 1) {
      this.setState(
        {
          page: this.state.page - 1,
          filter: null,
        },
        () => this.componentDidMount(),
      );
    } else {
      const setChores = response.status === 200 ? response.data.chores : [];
      const setPrev = response.status === 200 ? response.data.previous : false;
      const setNext = response.status === 200 ? response.data.next : false;

      this.setState({
        chores: setChores,
        prevPage: setPrev,
        nextPage: setNext,
      });
    }
  };

  render() {
    let noChoreText = '';
    let choresListText = '';
    switch (this.state.filter) {
      case true:
        noChoreText = 'completed';
        choresListText = 'Completed Chores List:';
        break;

      case false:
        noChoreText = 'incomplete';
        choresListText = 'Incomplete Chores List:';
        break;

      case null:
        noChoreText = '';
        choresListText = 'Chores List:';
        break;

      default:
        break;
    }
    return (
      <>
        <Modal
          open={this.state.isCreateModalOpen}
          closeModal={() =>
            this.setState({
              isCreateModalOpen: false,
            })
          }
        >
          <FillItem onSubmit={this.handleCreate} isCreate={true} item={null} />
        </Modal>

        <Modal
          open={this.state.isEditModalOpen}
          closeModal={() =>
            this.setState({
              isEditModalOpen: false,
            })
          }
        >
          <FillItem
            onSubmit={this.handleEdit}
            item={
              this.state.chores === null
                ? null
                : this.state.chores.find((c) => c.id === this.state.editChoreId)
            }
            isCreate={false}
          />
        </Modal>

        {this.state.chores !== null && this.state.chores.length === 0 ? (
          <>
            <h1 className="centered">There are no {noChoreText} chores.</h1>
            <div className="centered">
              <button
                className="btn btn-primary choreButton"
                onClick={() => {
                  this.setState({ filter: true, page: 1 }, () =>
                    this.componentDidMount(),
                  );
                }}
              >
                Filter completed
              </button>
              <button
                className="btn btn-primary choreButton"
                onClick={() => {
                  this.setState({ filter: false, page: 1 }, () =>
                    this.componentDidMount(),
                  );
                }}
              >
                Filter incomplete
              </button>
              <button
                className="btn btn-primary choreButton"
                onClick={() => {
                  this.setState({ filter: null, page: 1 }, () =>
                    this.componentDidMount(),
                  );
                }}
              >
                No filter
              </button>
            </div>
            <div className="centered">
              <button
                className="btn btn-primary"
                onClick={() => this.setState({ isCreateModalOpen: true })}
              >
                Add a chore
              </button>
            </div>
          </>
        ) : (
          this.state.chores !== null && (
            <>
              <h1 className="centered">{choresListText}</h1>
              <div className="centered">
                <button
                  className="btn btn-primary choreButton"
                  onClick={() => {
                    this.setState({ filter: true, page: 1 }, () =>
                      this.componentDidMount(),
                    );
                  }}
                >
                  Filter completed
                </button>
                <button
                  className="btn btn-primary choreButton"
                  onClick={() => {
                    this.setState({ filter: false, page: 1 }, () =>
                      this.componentDidMount(),
                    );
                  }}
                >
                  Filter incomplete
                </button>
                <button
                  className="btn btn-primary choreButton"
                  onClick={() => {
                    this.setState({ filter: null, page: 1 }, () =>
                      this.componentDidMount(),
                    );
                  }}
                >
                  No filter
                </button>
              </div>
              <div className="container">
                <div className="row">
                  {this.state.chores.map((chore) => (
                    <ListItem
                      key={chore.id}
                      item={chore}
                      handleComplete={this.handleCompleteButton}
                      handleDelete={this.handleDeleteButton}
                      editButtonClick={this.editItemButtonClick}
                    />
                  ))}
                </div>
              </div>
              <div className="centered">
                <button
                  className="btn btn-primary choreButton"
                  disabled={!this.state.prevPage}
                  onClick={() => {
                    this.setState(
                      {
                        page: this.state.page - 1,
                      },
                      () => this.componentDidMount(),
                    );
                  }}
                >
                  <span className="glyphicon glyphicon-arrow-left"></span>
                </button>
                <button
                  className="btn btn-primary choreButton"
                  disabled={!this.state.nextPage}
                  onClick={() => {
                    this.setState(
                      {
                        page: this.state.page + 1,
                      },
                      () => this.componentDidMount(),
                    );
                  }}
                >
                  <span className="glyphicon glyphicon-arrow-right"></span>
                </button>
              </div>
              <div className="centered">
                <button
                  className="btn btn-primary"
                  onClick={() => this.setState({ isCreateModalOpen: true })}
                >
                  <span className="glyphicon glyphicon-plus"></span>
                </button>
              </div>
            </>
          )
        )}
      </>
    );
  }
}

export default Main;
