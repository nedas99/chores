import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';

const initialValues = (item) => {
  if (!item) {
    return {
      title: '',
      description: '',
      date: moment().add(1, 'days').set({ second: 0 }),
    };
  }
  return {
    title: item.title,
    description: item.description,
    date: moment(item.date),
  };
};

const validationSchema = yup.object({
  title: yup.string().trim().required('Required'),
  date: yup.date().required('Required'),
});

const STYLE_MARGIN_TOP = {
  marginTop: '25px',
};

const STYLE_WIDTH = {
  width: '90%',
};

const FormikDateTimePicker = ({
  name,
  form: { setFieldValue },
  field: { value },
  ...rest
}) => {
  return (
    <DateTimePicker
      name={name}
      ampm={false}
      value={value}
      onChange={(value) => {
        setFieldValue('date', value);
      }}
      format="yyyy-MM-DD HH:mm"
      margin="normal"
      disableToolbar
      style={{ width: 'auto' }}
    />
  );
};

const FillItem = ({ isCreate, item, onSubmit }) => {
  if (!isCreate && !item)
    return <div>There's a problem loading a chore, please retry.</div>;

  const initVal = initialValues(item);

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <Formik
        initialValues={initVal}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        <Form style={{ padding: '5px' }}>
          <div style={STYLE_MARGIN_TOP}>
            <label htmlFor="title">Title</label>
            <div>
              <Field type="text" id="title" name="title" style={STYLE_WIDTH} />
            </div>
            <ErrorMessage
              name="title"
              render={(msg) => <div style={{ color: 'red' }}>{msg}</div>}
            />
          </div>
          <div style={STYLE_MARGIN_TOP}>
            <label htmlFor="description">Description</label>
            <div>
              <Field
                as="textarea"
                id="description"
                name="description"
                style={{ ...STYLE_WIDTH, resize: 'none' }}
              />
            </div>
            <ErrorMessage
              name="description"
              render={(msg) => <div style={{ color: 'red' }}>{msg}</div>}
            />
          </div>
          <div style={STYLE_MARGIN_TOP}>
            <label htmlFor="date">Date</label>
            <div>
              <Field name="date" component={FormikDateTimePicker} />
            </div>
            <ErrorMessage
              name="date"
              render={(msg) => <div style={{ color: 'red' }}>{msg}</div>}
            />
          </div>
          <div className="centered">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </Form>
      </Formik>
    </MuiPickersUtilsProvider>
  );
};

export default FillItem;
