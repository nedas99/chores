import React from 'react';

const ListItem = ({ item, handleComplete, handleDelete, editButtonClick }) => {
  const btnTitle = item.completed ? 'Mark as incomplete' : 'Mark as completed';
  const btnClass = item.completed
    ? 'btn btn-warning choreButton'
    : 'btn btn-success choreButton';
  const btnSpan = item.completed
    ? 'glyphicon glyphicon-off'
    : 'glyphicon glyphicon-ok';

  const panelClass = item.completed
    ? 'panel panel-default panel-completed'
    : 'panel panel-default';

  return (
    <div className="col-md-4 col-sm-6 col-xs-12">
      <div className={panelClass}>
        <div className="panel-heading">{item.title}</div>
        <table className="table">
          <tbody>
            <tr>
              <th>Description</th>
              <td>{item.description}</td>
            </tr>
            <tr>
              <th>Date</th>
              <td>{item.date.toLocaleString('lt-LT').replace('T', ' ')}</td>
            </tr>
            <tr>
              <th>Actions</th>
              <td>
                <button
                  type="button"
                  className={btnClass}
                  title={btnTitle}
                  onClick={() => handleComplete(item.id, !item.completed)}
                >
                  <span className={btnSpan}></span>
                </button>

                <button
                  type="button"
                  className="btn btn-primary choreButton"
                  title="Edit chore"
                  onClick={() => editButtonClick(item.id)}
                >
                  <span className="glyphicon glyphicon-pencil"></span>
                </button>

                <button
                  type="button"
                  className="btn btn-danger choreButton"
                  title="Delete chore"
                  onClick={() => handleDelete(item.id)}
                >
                  <span className="glyphicon glyphicon-remove"></span>
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ListItem;
