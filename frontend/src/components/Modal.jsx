import React from 'react';
import ReactDom from 'react-dom';

const MODAL_STYLES = {
  position: 'fixed',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  backgroundColor: '#DDD',
  padding: '50px',
  zIndex: 1000,
  width: '80%',
};

const OVERLAY_STYLES = {
  position: 'fixed',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(0, 0, 0, .7)',
  zIndex: 1000,
};

const Modal = ({ open, children, closeModal }) => {
  if (!open) return null;
  return ReactDom.createPortal(
    <>
      <div style={OVERLAY_STYLES} />
      <div style={MODAL_STYLES}>
        <div style={{ marginBottom: '40px' }}>
          <button className="btn btn-danger pull-right" onClick={closeModal}>
            <span className="glyphicon glyphicon-remove"></span>
          </button>
        </div>
        {children}
      </div>
    </>,
    document.getElementById('portal'),
  );
};

export default Modal;
