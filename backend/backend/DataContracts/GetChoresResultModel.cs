﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;

namespace backend.DataContracts
{
    public class GetChoresResultModel
    {
        public List<Chore> Chores { get; set; }
        public bool Previous { get; set; }
        public bool Next { get; set; }
    }
}
