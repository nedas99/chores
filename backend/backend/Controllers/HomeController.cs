﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Data;
using backend.Entities;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly DataContext _context;

        public HomeController(DataContext context)
        {
            _context = context;
        }

        [HttpPost("/populate")]
        public async Task<ActionResult> RunSeed()
        {
            try
            {
                Seed.Initialize(_context);
                return Ok();
            }
            catch
            {
                ModelState.AddModelError("database", "Something wrong populating the database");
                return BadRequest(ModelState);
            }
        }

        [HttpPost("/populate/{count}")]
        public async Task<ActionResult> AddChores(int count)
        {
            if (count < 1)
                return BadRequest();

            for (int i = 1; i <= count; i++)
            {
                var chore = new Chore
                {
                    Title = "title_" + i,
                    Description = "description_" + i,
                    Date = new DateTime(2022, 01, 01, 12, 00, 00),
                    Completed = false
                };
                _context.Chores.Add(chore);
            }
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("/delete")]
        public async Task<ActionResult> Delete()
        {
            try
            {
                var toDelete = _context.Chores;
                _context.Chores.RemoveRange(toDelete);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                ModelState.AddModelError("database", "Something wrong clearing the table");
                return BadRequest();
            }
        }
    }
}
