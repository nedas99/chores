﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using backend.Data;
using backend.Entities;
using backend.DataContracts;
using backend.Parameters;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChoresController : ControllerBase
    {
        private readonly DataContext _context;

        public ChoresController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Chores
        [HttpGet]
        public async Task<ActionResult<GetChoresResultModel>> GetChores(
            [FromQuery] PageParameters pageParams,
            [FromQuery] FilteringParameters filterParams
            )
        {
            if (pageParams.Page < 1 || pageParams.Size < 1)
            {
                ModelState.AddModelError("Bad parameters", "Query parameters should be positive integers");
                return BadRequest(ModelState);
            }

            List<Chore> chores;
            if (filterParams.Completed == null)
            {
                chores = await _context.Chores
                    .AsNoTracking()
                    .OrderBy(x => x.Date)
                    .Skip((pageParams.Page - 1) * pageParams.Size)
                    .Take(pageParams.Size)
                    .ToListAsync();
            }
            else
            {
                chores = await _context.Chores
                    .AsNoTracking()
                    .Where(x => x.Completed == (bool)filterParams.Completed)
                    .OrderBy(x => x.Date)
                    .Skip((pageParams.Page - 1) * pageParams.Size)
                    .Take(pageParams.Size)
                    .ToListAsync();
            }

            if (chores.Count == 0)
                return NoContent();

            int total;
            if (filterParams.Completed == null)
            {
                total = await _context.Chores
                    .AsNoTracking()
                    .CountAsync();
            }
            else
            {
                total = await _context.Chores
                    .AsNoTracking()
                    .Where(x => x.Completed == (bool)filterParams.Completed)
                    .CountAsync();
            }

            bool isPrevious = pageParams.Page > 1;
            bool isNext = pageParams.Page * pageParams.Size < total;

            return Ok(new GetChoresResultModel
            {
                Chores = chores,
                Previous = isPrevious,
                Next = isNext
            });
        }

        // GET: api/Chores/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Chore>> GetChore(int id)
        {
            var chore = await _context.Chores
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (chore == null)
            {
                return NotFound();
            }

            return Ok(chore);
        }

        // PUT: api/Chores/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChore(int id, InputChoreModel chore)
        {
            var choreFromDb = await _context.Chores
                .FirstOrDefaultAsync(x => x.Id == id);

            if (choreFromDb == null)
            {
                return NotFound();
            }

            if (chore.Date != null)
                choreFromDb.Date = (DateTime)chore.Date;

            if (!string.IsNullOrWhiteSpace(chore.Title))
                choreFromDb.Title = chore.Title;

            if (chore.Description != null)
                choreFromDb.Description = chore.Description;

            if (chore.Completed != null)
                choreFromDb.Completed = (bool)chore.Completed;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Chores
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Chore>> PostChore(InputChoreModel chore)
        {
            var choreToDb = new Chore
            {
                Title = chore.Title,
                Description = chore.Description,
                Date = (DateTime)chore.Date,
                Completed = (bool)chore.Completed
            };

            await _context.Chores.AddAsync(choreToDb);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChore", new { id = choreToDb.Id }, chore);
        }

        // DELETE: api/Chores/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChore(int id)
        {
            var chore = await _context.Chores
                .FirstOrDefaultAsync(x => x.Id == id);

            if (chore == null)
                return NotFound();

            _context.Chores.Remove(chore);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
