﻿using System;
using Microsoft.Data.Sqlite;
using backend.Data;
using Microsoft.EntityFrameworkCore;

namespace backend.Testing
{
    public class TestBase : IDisposable
    {
        private const string InMemoryConnectionString = "DataSource=:memory:";
        private readonly SqliteConnection conn;

        protected readonly DataContext DbContext;

        protected TestBase()
        {
            conn = new SqliteConnection(InMemoryConnectionString);
            conn.Open();

            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite(conn)
                .Options;

            DbContext = new DataContext(options);
            DbContext.Database.EnsureCreated();
        }

        public void Dispose()
        {
            conn.Close();
        }
    }
}
