﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Controllers;
using Xunit;
using backend.Entities;
using backend.DataContracts;
using backend.Parameters;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace backend.Testing
{
    public class ChoresControllerTests : TestBase
    {
        private readonly ChoresController controller;

        public ChoresControllerTests()
        {
            controller = new ChoresController(DbContext);
        }

        private List<Chore> GetChoreModels(int start, int end)
        {
            var list = new List<Chore>();

            for (int i = start; i <= end; i++)
            {
                list.Add(new Chore
                {
                    Title = "title_" + i,
                    Description = "desc_" + i,
                    Completed = true,
                    Date = new DateTime(2021, 01, 01, 12, 30, 00)
                });
            }

            return list;
        }

        private async void ClearChores()
        {
            var toDelete = DbContext.Chores;
            DbContext.Chores.RemoveRange(toDelete);
            await DbContext.SaveChangesAsync();
        }

        [Fact]
        public async Task GetChores_ReturnsOkAndNoOtherPages()
        {
            // Arrange:
            ClearChores();

            int start = 1;
            int end = 4;
            var chores = GetChoreModels(start, end);

            foreach(var chore in chores)
            {
                DbContext.Chores.Add(chore);
            }

            DbContext.SaveChanges();

            var pageParams = new PageParameters();
            var filterParams = new FilteringParameters();

            // Act:
            var response = await controller.GetChores(pageParams, filterParams);

            // Assert:
            var result = Assert.IsType<OkObjectResult>(response.Result);
            var model = Assert.IsType<GetChoresResultModel>(result.Value);

            Assert.True(!model.Previous);
            Assert.True(!model.Next);
            Assert.Equal(chores.Count, model.Chores.Count);

            foreach (var chore in chores)
            {
                Assert.Contains(model.Chores, x => x.Id == chore.Id);
            }
        }

        [Fact]
        public async Task GetChores_ReturnsNoContent()
        {
            // Arrange:
            ClearChores();

            var pageParams = new PageParameters();
            var filterParams = new FilteringParameters();

            // Act:
            var response = await controller.GetChores(pageParams, filterParams);

            // Assert:
            Assert.IsType<NoContentResult>(response.Result);
        }

        [Fact]
        public async Task GetChores_ReturnsOkAndLastPage()
        {
            // Arrange:
            ClearChores();

            var pageParams = new PageParameters();
            var filterParams = new FilteringParameters();
            pageParams.Page = 2;

            int start = 1;
            int end = pageParams.Size + 1;
            var chores = GetChoreModels(start, end);

            foreach (var chore in chores)
            {
                DbContext.Chores.Add(chore);
            }

            DbContext.SaveChanges();

            // Act:
            var response = await controller.GetChores(pageParams, filterParams);

            // Assert:
            var result = Assert.IsType<OkObjectResult>(response.Result);
            var model = Assert.IsType<GetChoresResultModel>(result.Value);

            Assert.True(model.Previous);
            Assert.True(!model.Next);
            Assert.Single(model.Chores);
        }

        [Fact]
        public async Task PutChore_ReturnsNoContentAndUpdatesChore()
        {
            // Arrange:
            ClearChores();

            int start = 1;
            int end = 3;
            var chores = GetChoreModels(start, end);

            foreach (var chore in chores)
            {
                DbContext.Chores.Add(chore);
            }

            DbContext.SaveChanges();
            var id = chores[1].Id;
            var choreToCompare = chores.FirstOrDefault(x => x.Id == id);

            // Act:
            InputChoreModel inputModel = new InputChoreModel
            {
                Title = "updated_title",
                Description = "updated_description",
                Date = new DateTime(2022, 01, 01, 00, 00, 00),
                Completed = false
            };

            var response = await controller.PutChore(id, inputModel);

            var fromDb = DbContext.Chores.FirstOrDefault(x => x.Id == id);

            // Assert:
            Assert.IsType<NoContentResult>(response);
            Assert.NotNull(fromDb);
            Assert.Equal(choreToCompare.Title, fromDb.Title);
            Assert.Equal(choreToCompare.Description, fromDb.Description);
            Assert.Equal(choreToCompare.Completed, fromDb.Completed);
            Assert.Equal(choreToCompare.Date.ToString(), fromDb.Date.ToString());
        }

        [Fact]
        public async Task DeleteChore_ReturnsNoContentAndSuccessfullyDeletesChore()
        {
            // Arrange:
            ClearChores();

            int start = 1;
            int end = 3;
            var chores = GetChoreModels(start, end);

            foreach (var chore in chores)
            {
                DbContext.Chores.Add(chore);
            }

            DbContext.SaveChanges();
            var id = chores[1].Id;
            var choreToCompare = chores.FirstOrDefault(x => x.Id == id);

            // Act:
            var response = await controller.DeleteChore(id);

            // Assert:
            var dbChores = DbContext.Chores.ToList();
            Assert.IsType<NoContentResult>(response);
            Assert.Equal(chores.Count - 1, dbChores.Count);
            Assert.DoesNotContain(dbChores, x => x.Id == id);
        }
    }
}
