﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using backend.Entities;

namespace backend.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        public DbSet<Chore> Chores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Chore>().ToTable("Chore");
        }
    }
}
