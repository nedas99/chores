﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;

namespace backend.Data
{
    public class Seed
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            FillChores(context);
        }

        static void FillChores(DataContext context)
        {
            if (context.Chores.Any())
                return;

            //var list = new List<Chore>();

            for (int i = 1; i <= 100; i++)
            {
                var chore = new Chore
                {
                    Title = "title_" + i,
                    Description = "description_" + i,
                    Date = new DateTime(2022, 01, 01, 12, 00, 00),
                    Completed = false
                };
                context.Chores.Add(chore);
                context.SaveChanges();
            }

            /*foreach (var a in list)
            {
                context.Chores.Add(a);
                context.SaveChanges();
            }*/
        }
    }
}
