﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Parameters
{
    public class PageParameters
    {
        public int Page { get; set; } = 1;
        public int Size { get; set; } = 12;
        /* Frontende padaryta, kad skirtingais ekrano
         * dydziais stulpelyje rodo 1, 2 arba 3 itemus,
         * tai reikia 6 kartotinio. */
    }

    public class FilteringParameters
    {
        public bool? Completed { get; set; } = null;
        /* true: filtruoti atliktus
         * false: filtruoti neatliktus 
         * null: nefiltruoti */
    }
}
